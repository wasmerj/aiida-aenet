from aiida.orm import load_node
from aiida.engine import submit

from aiida_aenet.calculations.train import AenetTrainCalculation

generate_calc = load_node(11002)

inputs = {
    "code": load_node(10739),
    "algorithm": generate_calc.inputs.algorithm,
    "train_file": generate_calc.outputs.train_file,
}

calc_node = submit(AenetTrainCalculation, **inputs)