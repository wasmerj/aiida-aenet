# aiida-aenet

A plugin for the [ænet](http://ann.atomistic.net) package with the [AiiDA](https://www.aiida.net) framework

# Installation

```shell
pip install aiida-aenet
```

# Implementation

aiida-aenet interfaces with ænet to construct interatomic potentials using its `generate.x`,
`train.x`, and `predict.x` tools:

- `aenet.generate`: Descriptor matrix calculation
- `aenet.train`: Neural-network weight matrix optimization
- `aenet.predict`: Neural-network potential performance testing

The `aenet.make_potential` WorkChain is available to automatically perform these operations
sequentially.

aiida-aenet stores algorithm hyperparameters with the `aenet.algorithm` custom data node
and stores potential data with the `aenet.potential` custom data node.

Potentials are compatible with [aiida-lammps](https://github.com/aiidaplugins/aiida-lammps) and can be simulated in classical molecular
dynamics using [aenet-lammps](https://github.com/HidekiMori-CIT/aenet-lammps) (an AENET build of LAMMPS) and`aenet.simulate`,
which uses `lammps.multi` with an ANN potential. The `aenet.compare_simulations` WorkChain
is available to automatically simulate and compare an ænet ANN potential and an empirical
LAMMPS potential.

# Potential Construction Example

```python
from aiida.orm import load_node, List
from aiida.engine import submit

from aiida_aenet.workflows.make_potential import MakePotentialWorkChain
from aiida_aenet.data.algorithm import AenetAlgorithm

elements = {
    "Ni": {
        "energy": -4676.3936784796315, # from reference
        "nodes": 2,
        "network": [{
            "nodes": 2,
            "activation": "tanh"
        }, {
            "nodes": 2,
            "activation": "tanh"
        }]
    },
    "P": {
        "energy": -194.14828627169916, # from reference
        "nodes": 2,
        "network": [{
            "nodes": 2,
            "activation": "tanh"
        }, {
            "nodes": 2,
            "activation": "tanh"
        }]
    }
}

chebyshev = {
    "type": "chebyshev",
    "parameters": {
        "radial_rc": 4.0,
        "radial_n": 6,
        "angular_rc": 4.0,
        "angular_n": 2
    },
}

parameters = {
    "test_percent": 10,
    "epochs": 10,
    "max_energy": 0.0,
    "r_min": 0.75,
}

nn_algorithm = AenetAlgorithm(
    elements=elements,
    descriptor=chebyshev,
    training="bfgs",
    parameters=parameters,
)

reference_pks = [
    <PwCalculation: uuid: ...>.pk
    <PwCalculation: uuid: ...>.pk
]

inputs = {
    'algorithm': nn_algorithm,
    'reference': List(list=reference_list),
    'generate': {'code': <Code: uuid: ...>},
    'train': {'code': <Code: uuid: ...>},
    'predict': {'code': <Code: uuid: ...>}
}

workchain = submit(MakePotentialWorkChain, **inputs)
```

# Current Version

aiida-aenet v0.1.0 supports:

- Artrith-Urban-Ceder (Chebyshev) descriptors
- Behler-Parrinello (symmetry function) descriptors
- L-BFGS-B training method (serial & parallel)

Future versions will support additional training methods, classical molecular dynamics plugin
compatibility, and other resources to assist with interatomic potential construction and analysis.

# References

ænet

- package citation: N. Artrith and A. Urban, Comput. Mater. Sci. 114 (2016) 135-150.
- atomic energy interpolation: J. Behler and M. Parrinello, Phys. Rev. Lett. 98 (2007) 146401.
- Chebyshev descriptors: N. Artrith, A. Urban, and G. Ceder, Phys. Rev. B 96 (2017) 014112.
- symmetry functions: J. Behler, J. Chem. Phys. 134 (2011) 074106.
- L-BFGS-B training: R. H. Byrd, P. Lu and J. Nocedal, SIAM J. Sci. Stat. Comp. 16 (1995) 1190-1208.

ænet-lammps

- package citation: H. Mori, and T. Ozaki, Phys. Rev. Mater. 4(4) (2020) 040601.
- LAMMPS: Thompson, Aidan P., et al. Comp. Phys. Comm. 271 (2022) 108171.

AiiDA

- S.P. Huber et al., Scientific Data 7, 300 (2020)
- M. Uhrin et al., Comp. Mat. Sci. 187 (2021)
